# Secret Notes

Secret Notes is a NestJS based API Server developed as a part of coding challenge provided by Spherity.

Secret Notes contains CRUD Endpoints to interact with an entity called SecretNote. There is no user management available. The server interacts with the entity Note in such a way that when user makes a post request to create a note. The server encrypts the 'note' attribute using AES-256 (Advanced Encryption System 256) and saves it in hex in a local mongoDB.


## Description
###### Coding Language: Typescript
###### Framework: NestJS
###### Database: Dockerized MongoDB

## How to run

Use docker-compose to run and build.

```bash
docker-compose up --build
```

Use docker-compose to build.

```bash
docker-compose build
```

Use docker-compose to run.

```bash
docker-compose up
```

## API Details
#### Note Object
| Attributes | Type |
| ------ | ------ |
| ID | String (Unique Auto Generated) |
| Title | String |
| Author | String |
| Note | String (Encryted using AES-256 Hex) |
| InitVector | String (Random Auto Generated Hex) |
| Secret_Code | String |
| createdAt | Date

#### API ENDPOINTS
`GET: http://{host}/api/secret-note/`

`GET: http://{host}/api/secret-note/:id/`
Input:
| Param | Type | Required |
| ------ | ------ | ------ |
| ID | String | Yes |

`GET: http://{host}/api/secret-note/:id/decrypted?secretcode={string}`
Input:
| Param | Type | Required |
| ------ | ------ | ------ |
| ID | String | Yes |

| Query | Type | Required |
| ------ | ------ | ------ |
| secretCode | String | No |

`POST: http://{host}/api/secret-note/`
| Body | Type | Required |
| ------ | ------ | ------ |
| Author | String | Yes |
| Note | String | Yes |
| Title | String | Yes |
| SecretCode | String | No |

`PATCH: http://{host}/api/secret-note/:id?secretcode={string}/`
| Param | Type | Required |
| ------ | ------ | ------ |
| ID | String | Yes |

| Query | Type | Required |
| ------ | ------ | ------ |
| secretCode | String | No |

| Body | Type | Required |
| ------ | ------ | ------ |
| Author | String | No |
| Note | String | No |
| Title | String | No |
| SecretCode | String | No |

`DELETE: http://{host}/api/secret-note/:id?secretcode={string}//`
| Param | Type | Required |
| ------ | ------ | ------ |
| ID | String | Yes |

| Query | Type | Required |
| ------ | ------ | ------ |
| secretCode | String | No |

`DELETE: http://{host}/api/secret-note/`
