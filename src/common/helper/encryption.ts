import { createCipheriv, createDecipheriv, randomBytes, scryptSync } from 'crypto';

/*
    Encryption Class
    Methods:
        createIV
        encryptMessage
        decryptMessage
*/
export class Encryption {

    /*
    createIV:
        Description: Creates random 16 bytes in hex to store as the initialization vector for the encrypted note.
    */
    createIV() {
        return randomBytes(16).toString('hex');
    }

    /*
    encryptMessage:
        Description: Encrypts a given message using AES-256 and it's Initialization Vector and Password.
        Input:
            message: string,
            iv: string,
            password: string,
        Output:
            encryptedText: string,
    */
    encryptMessage(message: string, iv: string, password: string) {
        const stringAsBytes = Buffer.from(iv, 'hex');
        console.log(iv);
        const key = scryptSync(password, 'salt', 32);
        const cipher = createCipheriv('aes-256-ctr', key, stringAsBytes);
        const encryptedText = cipher.update(message, 'utf-8', 'hex');
        return encryptedText;
    }

    /*
    decryptMessage:
        Description: Decrypts and encrypted Message using it's initialization vector and password.
        Input:
            encryptedText: string,
            iv: string,
            password: string,
        Output:
            decryptedText: string,
    */
    decryptMessage(encryptedText: string, iv: string, password: string) {
        const stringAsBytes = Buffer.from(iv, 'hex');
        const key = scryptSync(password, 'salt', 32);
        const decipher = createDecipheriv('aes-256-ctr', key, stringAsBytes);
        const decryptedText = decipher.update(encryptedText, 'hex', 'utf-8');
        return decryptedText;
    }
}