import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';

@Schema()
export class SecretNote extends Document {

    @Prop({required: true})
    title: string;

    @Prop({required: true})
    author: string;

    @Prop({required: true})
    note: string;

    @Prop({required: false})
    initvector: string;

    @Prop({required: false})
    secret_code: string;

    @Prop({ default: Date.now })
    createdAt: Date;
}

export const SecretNoteSchema = SchemaFactory.createForClass(SecretNote);