import { Injectable } from '@nestjs/common';
import { CreateSecretNoteDto } from './dto/create-secret-note.dto';
import { UpdateSecretNoteDto } from './dto/update-secret-note.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { SecretNote } from './entities/secret-note.entity';
import { Encryption } from 'src/common/helper/encryption';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SecretNoteService {
  constructor(
    @InjectModel(SecretNote.name) private readonly secretNoteModel: Model<SecretNote>,
    private readonly encryption: Encryption,
    private readonly configService: ConfigService
  ) {}

  async create(createSecretNoteDto: CreateSecretNoteDto) {
    createSecretNoteDto.initvector = this.encryption.createIV();
    if (!createSecretNoteDto.secret_code) {
      createSecretNoteDto.secret_code = this.configService.get<string>('PUBLIC_KEY');
    } 
    createSecretNoteDto.note = this.encryption.encryptMessage(createSecretNoteDto.note, createSecretNoteDto.initvector, createSecretNoteDto.secret_code);
    const createdNote = await this.secretNoteModel.create(createSecretNoteDto);
    const res = this.secretNoteModel.findById(createdNote._id).select('-secret_code -initvector');
    return res;
  }

  findAll() {
    return this.secretNoteModel.find().select('-secret_code -initvector').exec();
  }

  async findOne(id: string) {
    return await this.secretNoteModel.findById(id).select('-secret_code -initvector').exec();
  }

  async findOneDecrypted(id: string) {
    const note = await this.secretNoteModel.findById(id).exec();
    const res = await this.secretNoteModel.findById(id).select('-secret_code -initvector').exec();
    if (note) {
      res.note = this.encryption.decryptMessage(note.note, note.initvector, note.secret_code);
    }
    return res;
  }
  async update(id: string, updateSecretNoteDto: UpdateSecretNoteDto) {
    let updatedSecretNote = null;
    if (updateSecretNoteDto.note) {
      let key = null;
      if (!updateSecretNoteDto.secret_code) {
        key = (await this.secretNoteModel.findById(id)).secret_code;
      } else {
        key = updateSecretNoteDto.secret_code;
      }
      updateSecretNoteDto.initvector = this.encryption.createIV();
      updateSecretNoteDto.note = this.encryption.encryptMessage(updateSecretNoteDto.note, updateSecretNoteDto.initvector, key);
      updatedSecretNote = await this.secretNoteModel.findByIdAndUpdate(id, updateSecretNoteDto).select('-secret_code -initvector');
    } else {
      updatedSecretNote = await this.secretNoteModel.findByIdAndUpdate(id, updateSecretNoteDto).select('-secret_code -initvector');
    }
    return updatedSecretNote;
  }

  async remove(id: string) {
    return await this.secretNoteModel.findByIdAndRemove(id).select('-secret_code -initvector').exec();
  }

  async removeAll() {
    return await this.secretNoteModel.remove().select('-secret_code -initvector');
  }

  async checkSecretCode(id: string, secret_code: string) {
    const note = await this.secretNoteModel.findById(id);
    if (note.secret_code === secret_code) {
      return true;
    } else {
      return false;
    }
  }
}
