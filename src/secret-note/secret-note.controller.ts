import { Controller, Get, Post, Body, Patch, Param, Delete, Query, HttpStatus, HttpException } from '@nestjs/common';
import { SecretNoteService } from './secret-note.service';
import { CreateSecretNoteDto } from './dto/create-secret-note.dto';
import { UpdateSecretNoteDto } from './dto/update-secret-note.dto';
import { ConfigService } from '@nestjs/config';

@Controller('secret-note')
export class SecretNoteController {
  constructor(
              private readonly secretNoteService: SecretNoteService,
              private readonly configService: ConfigService
  ) {}

  /*
    TYPE: POST
    DESCRIPTION: Post request to create a secret-note.
    INPUT:
      Params: None
      QueryParams: None
      Body:
        author: String <Required>
        title: String <Required>
        note: String <Required>
        secret_code: String <Optional>
  */
  @Post()
  create(@Body() createSecretNoteDto: CreateSecretNoteDto) {
    if (!createSecretNoteDto.note) {
      throw new HttpException('Note is a required field', HttpStatus.BAD_REQUEST);
    }
    if (!createSecretNoteDto.author) {
      throw new HttpException('Author is a required field', HttpStatus.BAD_REQUEST);
    }
    if (!createSecretNoteDto.title) {
      throw new HttpException('Title is a required field', HttpStatus.BAD_REQUEST);
    }
    return this.secretNoteService.create(createSecretNoteDto);
  }

  /*
    TYPE: GET
    DESCRIPTION: GET ALL Request to get all notes in encrypted format.
    INPUT:
      Params: None
      QueryParams: None
      Body: None
  */
  @Get()
  findAll() {
    const secretNotes = this.secretNoteService.findAll();
    if (secretNotes) {
      return secretNotes;
    } else {
      throw new HttpException('No Notes Found', HttpStatus.NOT_FOUND);
    }
  }

  /*
    TYPE: GET
    DESCRIPTION: GET a single request in a decrypted format.
    INPUT:
      Params:
        id: String <Required>
        decrypt: String <Required> 
      QueryParams:
        secretcode: String <Optional>
      Body: None
  */
  @Get(':id/:decrypt')
  async findOne(@Param('id') id: string, @Param('decrypt') encryption: string, @Query('secretcode') secretCode: string) {
    if (encryption === 'decrypted') {
      const note = await this.secretNoteService.findOneDecrypted(id);
      if (note) {
        if (!secretCode) {
          secretCode = this.configService.get<string>('PUBLIC_KEY');
        }
        if (await this.secretNoteService.checkSecretCode(id, secretCode)) {;
          return note;
        } else {
          throw new HttpException('Invalid Secret Code', HttpStatus.FORBIDDEN);
        }
      } else {
        throw new HttpException('Note not found', HttpStatus.NOT_FOUND);
      }
    } else {
      throw new HttpException('Invalid Route, did you mean /decrypted?', HttpStatus.BAD_REQUEST);
    }
  }

  /*
    TYPE: GET
    DESCRIPTION: GET a single request in an encrypted Format.
    INPUT:
      Params:
        id: String <Required>
      Body: None
  */
  @Get(':id/')
  async findOneEncrypted(@Param('id') id: string) {
    const note = await this.secretNoteService.findOne(id);
    if (note) {
      return note;
    } else {
      throw new HttpException('Note not found', HttpStatus.NOT_FOUND);
    }
  }

  /*
    TYPE: PATCH
    DESCRIPTION: Update a single secret-note
    INPUT:
      Params:
        id: String <Required>
      QueryParams:
        secretcode: String <Optional>
      Body:
        author: String <Optional>
        title: String <Optional>
        note: String <Optional>
        secret_code: String <Optional>
  */
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateSecretNoteDto: UpdateSecretNoteDto, @Query('secretcode') secretCode: string) {
    const foundNote = await this.secretNoteService.findOne(id);
    if (foundNote) {
      if (!secretCode) {
        secretCode = this.configService.get<string>('PUBLIC_KEY');
      }
      if (await this.secretNoteService.checkSecretCode(id, secretCode)) {
        const updatedNote = await this.secretNoteService.update(id, updateSecretNoteDto);
        return updatedNote;
      } else {
        throw new HttpException('Invalid Secret Code', HttpStatus.FORBIDDEN);
      }
    } else {
      throw new HttpException('Note not found', HttpStatus.NOT_FOUND);
    }
  }

  /*
    TYPE: DELETE
    DESCRIPTION: Delete a single secret-note
    INPUT:
      Params:
        id: String <Required>
      QueryParams:
        secretcode: String <Optional>
      Body: None
  */
  @Delete(':id')
  async remove(@Param('id') id: string, @Query('secretcode') secretCode: string) {
    if (await this.secretNoteService.findOne(id)) {
      if (!secretCode) {
        secretCode = this.configService.get<string>('PUBLIC_KEY');
      }
      if (await this.secretNoteService.checkSecretCode(id, secretCode)) {
        const deletedNote = await this.secretNoteService.remove(id);
        if (deletedNote) {
          return deletedNote;
        }
      } else {
        throw new HttpException('Invalid Secret Code', HttpStatus.FORBIDDEN);
      }
    } else {
      throw new HttpException('Note not found', HttpStatus.NOT_FOUND);
    }
  }

  /*
    TYPE: DELETE
    DESCRIPTION: Delete all secret-notes
    INPUT:
      Params: None
      QueryParams: None
      Body: None
  */
  @Delete()
  async removeAll() {
    const deletedSecretNotes = await this.secretNoteService.removeAll();
    if (deletedSecretNotes) {
      return deletedSecretNotes;
    } else {
      throw new HttpException('No Notes Found', HttpStatus.NOT_FOUND);
    }
  }
}
