export class CreateSecretNoteDto {
    readonly title: string;
    readonly author: string;
    note: string;
    initvector: string;
    secret_code: string;
}
