import { Module } from '@nestjs/common';
import { SecretNoteService } from './secret-note.service';
import { SecretNoteController } from './secret-note.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SecretNote, SecretNoteSchema } from './entities/secret-note.entity';
import { Encryption } from 'src/common/helper/encryption';

@Module({
  imports: [MongooseModule.forFeature([{ name: SecretNote.name, schema: SecretNoteSchema }])],
  controllers: [SecretNoteController],
  providers: [SecretNoteService, Encryption]
})
export class SecretNoteModule {}
